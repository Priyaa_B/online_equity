<?php

$stocks = $query->SelectQuery('SELECT * FROM stocks');
$i=1;
$ch = curl_init();
foreach($stocks as $stock){

$curlConfig = array(
    CURLOPT_URL            => "https://finnhub.io/api/v1/quote?symbol=".$stock['symbol']."&token=".STOCK_SEARCH_API_KEY,
    // CURLOPT_POST           => true,
    CURLOPT_RETURNTRANSFER => true,
);
curl_setopt_array($ch, $curlConfig);
$stock_list[$i] = json_decode(curl_exec($ch),true);
$stock_list[$i]['stock_id'] = $stock['id'];
$stock_list[$i]['stock_name'] = $stock['name'];
$stock_list[$i]['logo'] = $stock['images'];
$stock_list[$i++]['symbol'] = $stock['symbol'];


}

?>