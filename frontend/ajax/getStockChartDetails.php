<?php
include(dirname(__DIR__) . "/classes/SqlQueries.php");
$query = new SqlQueries();
$stock = "ORCL";
if(isset($_POST['symbol']) && !empty($_POST['symbol'])){

    $stock =$_POST['symbol'];
}
$stocks = $query->SelectSingle('SELECT * FROM stocks WHERE symbol="'.$stock.'"');
$i=1;
$ch = curl_init();

$curlConfig = array(
    CURLOPT_URL            => "http://api.marketstack.com/v1/eod?access_key=".STOCK_LIST_API_KEY."&symbols=".$stock,
    // CURLOPT_POST           => true,
    CURLOPT_RETURNTRANSFER => true,
);
curl_setopt_array($ch, $curlConfig);
$record = json_decode(curl_exec($ch),true);
$record['stock_name'] = $stocks['name'];
$record['symbol'] = $stock;
$stockName = $stocks['name'];
$symbol = $stock;
$single_stock_list = array();
// print_r($record);
// exit;

foreach($record['data'] as $key => $stockList){
$single_stock_list[$key]['open'] = $record['data'][$key]['open'];
$single_stock_list[$key]['high'] = $record['data'][$key]['high'];
$single_stock_list[$key]['low'] = $record['data'][$key]['low'];
$single_stock_list[$key]['close'] = $record['data'][$key]['close'];
$single_stock_list[$key]['date'] = date('Y-m-d',strtotime($stockList['date']));
// $single_stock_list['stock_name'] = $record['stock_name'];
// $single_stock_list['symbol'] = $record['symbol'];
}
echo json_encode($single_stock_list);