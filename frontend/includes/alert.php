<div class="container-fluid">
    <?php if (isset($_SESSION['level'])) { ?>
    <div class="alert alert-<?php echo $_SESSION['level']; ?> alert-dismissible fade show" role="alert"> <?php echo $_SESSION['message']; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>
</div>