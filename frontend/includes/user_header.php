<header>
	<!-- <div class="container-fluid"> -->
		<nav class="navbar navbar-expand-lg navbar-light ">
			<a class="navbar-brand" href="<?php echo BASE_PATH?>/index.php"><img src="<?php echo BASE_PATH ?>/frontend/assets/imgs/logo-dark.png" class="logo"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="<?php echo BASE_PATH?>/user_index.php">Home <span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link" href="<?php echo BASE_PATH?>/user/portfolio.php">Portfolio</a></li>
					<li class="nav-item"><a class="nav-link" href="<?php echo BASE_PATH?>/user/wallet.php">Wallet</a></li>
					
					<li class="nav-item desktop-menu"><a class="nav-link" href="#">Logout</a></li>
				</ul>
		  	</div>
		  	<ul class="navbar-nav ml-auto mobile-menu">
				  <?php if(isset($_SESSION['user_session'])){ ?>
					<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="<?php echo BASE_PATH ?>/frontend/assets/imgs/user_icon.png"/> <?php echo $_SESSION['user_session']['name'] ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a href="<?php echo BASE_PATH?>/logout.php" class="dropdown-item" href="#">Logout</a>
        </div>
      </li>
				  <?php }else{ ?>
				  <li class="nav-item"><a class="nav-link" href="signin.php">Sign In</a></li>
				  <li class="nav-item"><a class="nav-link" href="signup.php">Sign Up</a></li>
				  <?php } ?>
              </ul>
		</nav>

	<!-- </div> -->
</header>