<?php

if( isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5  ){
    unset($_SESSION['level'] ,$_SESSION['message']);
    unset($_SESSION['u_time']);
}
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<link rel="stylesheet" type="text/css" href="<?php echo BASE_PATH?>/frontend/assets/css/bootstrap/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_PATH?>/frontend/assets/css/custom.css">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_PATH?>/frontend/assets/css/font-awesome-4/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_PATH?>/frontend/assets/docs/css/ionicons.min.css">
<!-- <script>
    window.onload = function() {
 
 var chart = new CanvasJS.Chart("chartContainer", {
     title: {
         text: "Ericsson Stock Price - December 2017"
     },
     subtitles: [{
         text: "Currency in Swedish Krona"
     }],
     axisX: {
         valueFormatString: "DD MMM",
         crosshair: {
			enabled: true,
			snapToDataPoint: true
		}
     },
     axisY: {
         suffix: " kr",
        //  includeZero: true,
		crosshair: {
			enabled: true,
			snapToDataPoint: true
		}
     },
     data: [{
         type: "candlestick",
         xValueType: "dateTime",
         yValueFormatString: "#,##0.0 kr",
         xValueFormatString: "DD MMM",
         dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
     }]
 });
 chart.render();
 }
</script> -->
</head>
<body>