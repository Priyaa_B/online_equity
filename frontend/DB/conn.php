<?php
require_once(dirname(__DIR__)."/classes/constants.php");
class DBConnection
{
    function getdbconnect(){
        $con = mysqli_connect("localhost",DB_USER,DB_PASS,DB_NAME);
        // Check connection
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            exit();
        }
        return $con;
    }
}