<?php
require_once(__DIR__.'\frontend/ajax/getStocks.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="frontend\assets\bootstrap5\css\bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="frontend\assets\jquery\jquery.min.js"></script>
    <script type="text/javascript" src="frontend\assets\bootstrap5\js\bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <form class="row g-3">

        <div class="col-md-6">
            <label for="inputCity" class="form-label">City</label>
            <input type="text" class="form-control" id="stocks">
        </div>
        <div class="col-md-4">
            <label for="inputState" class="form-label">Stocks</label>
            <select id="stocks123" class="form-select">
                <option selected>Choose...</option>
                <option>...</option>
            </select>
        </div>
        <div class="col-12">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Check me out
                </label>
            </div>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary">Sign in</button>
        </div>
    </form>
    <table class="table" id="stock-list">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">LOGO</th>
      <th scope="col">NAME</th>
      <th scope="col">Current Price</th>
      <th scope="col">Opening Price</th>
      <th scope="col">Closing Price</th>
      <th scope="col">Closing Price</th>
    </tr>
  </thead>
  <tbody>
  <?php $i=1; 
  foreach($stock_list as $stock){?>
  <tr>
      <th scope="row"><?php echo $i++; ?></th>
      <td><?php echo "-" ?></td>
      <td><?php echo $stock['stock_name'] ?></td>
      <td><?php echo $stock['c'] ?></td>
      <td><?php echo $stock['o'] ?></td>
      <td><?php echo $stock['pc'] ?></td>
    </tr>
  
  <?php } ?>
  </tbody>
</table>
</div>
<script>
// $(document).ready(function(){
//     $.ajax({
//   type: "GET",
//   url: "frontend/ajax/getStocks.php",
// //   data: {keyword:key},
//   cache: false,
//   success: function(data){
//       console.log(data);
//       var sno=1;
//     //   $.map( data, function( val, i ) {
//     //     var html = '<tr>'+
//     //   '<th scope="row">'+(i++) +'</th>'+
//     //   '<td>Mark</td>'+
//     //   '<td>Otto</td>'+
//     //   '<td>@mdo</td>'+
//     //   '<td>@mdo</td>'+
//     //   '<td>@mdo</td>'+
//     // '</tr>';
//     //  $("#stock-list tbody").append(data);
// }
//   });
// })

</script>
</body>

</html>