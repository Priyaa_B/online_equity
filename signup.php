<?php
session_start();
include(__DIR__ . '/frontend/classes/SqlQueries.php');
$sqlQuery = new SqlQueries();

if (isset($_SESSION['user_session'])) {
    redirect('index.php');
}
$role_id = CUSTOMER_ROLE_ID;
$more_fields = "";
$table = "";
$state_id = '';
$city_id = '';
if (isset($_POST['register'])) {
$user_check = $sqlQuery->CountQuery('SELECT * FROM customers WHERE email="'.$_POST['emailid'].'"');

if($user_check >0){
	echo "<script>alert('Email Id already exists. Please log in')</script>";
    redirect('signin.php');
}else{
    $table = 'customers';
    $role_id = CUSTOMER_ROLE_ID;
    $more_fields = ", address = '" . $_POST['address'] . "' ";

    $UserId = $sqlQuery->InsertQuery('INSERT INTO users SET role_id="' . $role_id . '" ,name="' . $_POST['fname'] . '" ,username = "' . $_POST['username'] . '", password = "' . $_POST['password'] . '", encrypted_password = "' . md5($_POST['password']) . '"');

    $customer_user_Id = $sqlQuery->InsertQuery("INSERT INTO 
" . $table . " SET user_id = '" . $UserId . "', first_name =  '" . $_POST['fname'] . "', last_name = '" . $_POST['lname'] . "',  age = '" . $_POST['age'] . "', gender = '" . $_POST['gender'] . "', contact_no = '" . $_POST['contact_no'] . "', email = '" . $_POST['emailid'] . "' {$more_fields }");

    echo "<script>alert('Account created successfully. Please login to your account to proceed further.')</script>";
    redirect('signin.php');
}
}

?>
<?php include(__DIR__ . '/frontend/includes/head.php') ?>
<?php include(__DIR__ . '/frontend/includes/header.php') ?>

<div class="d-flex justify-content-center">
	<div class="signUp my-auto">
			<form method="post">
			<span>Create Account</span>
			<div class="row">
			<div class="col-md-6">
			<div class="form-group">
  				<input type="text" class="form-control" name="fname" placeholder="First Name">
			</div>	
			</div>	
			<div class="col-md-6">
			<div class="form-group">
  				<input type="text" class="form-control" name="lname" placeholder="Last Name">
			</div>	
			</div>	
			</div>
			<div class="row">
			<div class="col-md-6">
			<div class="form-group">
  				<input type="text" class="form-control" name="age" placeholder="Age">
			</div>	
			</div>
			<div class="col-md-6">	
			<div class="form-group">
  				<input type="text" class="form-control" name="gender" placeholder="Gender">
			</div>
			</div>
			</div>
			<div class="form-group">
  				<input type="text" class="form-control" name="contact_no" placeholder="Contact">
			</div>	
			
			<div class="form-group">
  				<input type="email" class="form-control" name="emailid" placeholder="Email Address">
			</div>	
			<div class="form-group">
  				<textarea class="form-control" name="address" placeholder="Address"></textarea>
			</div>	
			<div class="form-group">
  				<input type="text" class="form-control" name="username" placeholder="Username">
			</div>
			<div class="form-group">
  				<input type="password" class="form-control" name="password" placeholder="Password">
			</div>
			<!-- <div class="custom-control custom-checkbox">
  				<input type="checkbox" class="custom-control-input" id="form-checkbox">
  				<label class="custom-control-label" for="form-checkbox">I agree to the <a href="#!">Terms &amp; Conditions</a></label>
			</div> -->
			<button type="submit" name="register" class="btn btn-primary">Create Account</button>
			</form>
			<h2>Already have an account? <a href="signin.php">Sign in here</a></h2>
	</div>
	</div>


	<?php include(__DIR__ . '/frontend/includes/footer.php') ?>
</body>
</html>