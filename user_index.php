<?php
session_start();
if (isset($_SESSION['stock_list']['date']) && $_SESSION['stock_list']['date'] <= strtotime(date("Y-m-d H:i:s"))) {

	unset($_SESSION['stock_list']);
}
include(__DIR__ . "/frontend/classes/SqlQueries.php");
$query = new SqlQueries();
$stocks_listing = array();
if (!isset($_SESSION['stock_list']) || count($_SESSION['stock_list']) == 0) {
	require_once(__DIR__ . '\frontend/ajax/getStocks.php');
	$_SESSION['stock_list'] = $stock_list;
	$_SESSION['stock_list']['date'] = strtotime(date('Y-m-d') . "23:59:59");
}
$stocks_listing = $_SESSION['stock_list'];

//customer details
if (isset($_POST['buy_stock'])) {
	unset($_SESSION['stock_details']['stock_symbol']);
	$_SESSION['u_time'] = time();

	$total_price = $_POST['no_of_shares'] * $_POST['stock_price'];

	$customer_wallet = $query->SelectSingle("SELECT * FROM customers WHERE customer_id=" . $_POST['customer_id']);
	if ($customer_wallet['wallet_amount'] == null || $customer_wallet['wallet_amount'] == 0 || $customer_wallet['wallet_amount'] == "" || $customer_wallet['wallet_amount'] < $total_price) {
		$_SESSION['level'] = 'danger';
		$_SESSION['message'] = 'Insufficient balance please check';
		redirect('user_index.php');
	} elseif ($customer_wallet['wallet_amount'] >= $total_price) {

		$stock_details = $query->SelectSingle("SELECT * FROM stocks WHERE id=" . $_POST['stock_id']);

		$customer_stock_id = $query->InsertQuery('INSERT INTO customer_stocks SET customer_id="' . $_POST['customer_id'] . '", stock_id="' . $_POST['stock_id'] . '", stock_price="' . $_POST['stock_price'] . '", quantity="' . $_POST['no_of_shares'] . '", total_price="' . $total_price . '", create_date="' . date('Y-m-d H:i:s') . '"');
		$customer_stock_activity_id = $query->InsertQuery('INSERT INTO customer_stock_activities SET customer_stock_id="' . $customer_stock_id . '", customer_id="' . $_POST['customer_id'] . '",action =' . ACTION_BUY . ', quantity="' . $_POST['no_of_shares'] . '", total_price="' . $total_price . '", create_date="' . date('Y-m-d H:i:s') . '"');
		$left_amount = $customer_wallet['wallet_amount'] - $total_price;
		$query->UpdateQuery("UPDATE customers SET wallet_amount =" . $left_amount . " WHERE customer_id=" . $_POST['customer_id']);
		$query->InsertQuery("INSERT INTO wallet_history SET customer_id =" . $_POST['customer_id'] . ", customer_stock_activity_id=" . $customer_stock_activity_id . ",type=" . TYPE_WITHDRAWN . ", amount=" . $total_price . ",action='" . WITHDRAWN_TEXT . ' ' . $stock_details['symbol'] . "',create_date='" . date('Y-m-d H:i:s') . "'");

		if (!empty($customer_stock_activity_id)) {
			$_SESSION['level'] = 'success';
			$_SESSION['message'] = 'Stock bought successfully';
		} else {
			$_SESSION['level'] = 'danger';
			$_SESSION['message'] = 'Record cannot be added';
			$message = "Record cannot be added";
		}
		redirect('/user/portfolio.php');
	}
}

$customer_stocks = $query->SelectQuery("SELECT * FROM customer_stocks WHERE customer_id=" . $_SESSION['user_session']['customer_id']);
$single_stock_list = array();

?>
<?php include(__DIR__ . '/frontend/includes/head.php') ?>
<?php include(__DIR__ . '/frontend/includes/user_header.php') ?>

<div class="container-fluid mtb15 no-fluid">
	<?php include('frontend\includes\alert.php') ?>
	<div class="row sm-gutters">
		<div class="col-md-12 col-lg-4">
			<div class="tab overflow-auto">
				<h2 class="text-center">Stocks</h2>
				<?php
				$i = 1;
				foreach ($stocks_listing as $key => $stocks) { ?>
					<button class="<?php echo (isset($_SESSION['stock_details']['stock_symbol']) && $stocks['symbol'] == $_SESSION['stock_details']['stock_symbol'] ? 'tablinks active' : 'tablinks') ?>" onclick="openStock(event, 'Tab<?php echo $i ?>')" data-tab-id="<?php echo $i++ ?>" data-symbol="<?php echo $stocks['symbol'] ?>"" id=<?php echo $key == 1 ? "defaultOpen" : "" ?>>
						<div class=" walBox d-flex justify-content-between align-items-center">
						<div class="d-flex">
							<img src="frontend/assets/imgs/stock_logos/<?php echo $stocks['logo'] ?>">
							<div>
								<h2><?php echo $stocks['symbol'] ?></h2>
								<p><?php echo $stocks['stock_name'] ?></p>
							</div>
						</div>
						<div>
							<h3><?php echo $stocks['c'] ?></h3>
							<!-- <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p> -->
						</div>
			</div>
			</button>
			<hr>
		<?php } ?>
		</div>
	</div>
	<div class="col-md-12 col-lg-8">
		<?php
		$inc = 1;
		foreach ($stocks_listing as $key => $stocks) {

		?>
			<div id="Tab<?php echo $inc ?>" data-inc-id =<?php echo $inc ?> class="tabcontent <?php echo (isset($_SESSION['stock_details']['stock_symbol']) && $stocks['symbol'] == $_SESSION['stock_details']['stock_symbol'] ? 'active' : '') ?>">
				<div class="walContBox">
				<div class="card">
						<div class="card-body">

							<h5 class="card-title"><?php echo $stocks['symbol'] ?></h5>
							<div class="row wallAddr">
								<div class="col-md-12">
									<div id="chartdiv<?php echo  $inc++ ?>" style="height: 470px; width: 100%;" data-div-id="<?php echo $key; ?>"></div>
								</div>

							</div>
						</div>
					</div>	
				<div class="card">
						<form method="post">
							<div class="card-body">
								<h5 class="card-title"><?php echo $stocks['symbol'] ?></h5>
								<ul>
									<li class="d-flex justify-content-between align-items-center">
										<div class="d-flex align-items-center">
											<i class="icon ion-md-cash"></i>
											<h2>Shares</h2>
										</div>
										<div>
											<input class="form-control" type="hidden" name="stock_id" value="<?php echo $stocks['stock_id'] ?>" />
											<input class="form-control" type="hidden" name="customer_id" value="<?php echo $_SESSION['user_session']['customer_id'] ?>" />
											<input class="form-control" type="number" name="no_of_shares" placeholder="Enter no of shares" required />
										</div>
									</li>
									<li class="d-flex justify-content-between align-items-center">
										<div class="d-flex align-items-center">
											<i class="icon ion-md-checkmark"></i>
											<h2>Price</h2>
										</div>
										<div>
											<input class="form-control" type="hidden" name="stock_price" value="<?php echo $stocks['c'] ?>" />
											<h3><?php echo $stocks['c'] ?> <?php echo $stocks['symbol'] ?></h3>
										</div>
									</li>
								</ul>
								<button type="submit" name="buy_stock" class=" form-control btn green">Buy</button>
								<!-- <button type="submit" name="sell_stock" class="btn red">Sell</button> -->
							</div>
						</form>
					</div>
					

					<!--<div class="card">
							<div class="card-body">
								<h5 class="card-title">Latest Transactions</h5>
								<div class="wallet-history">
									<table class="table">
										<thead>
											<tr>
												<th>No.</th>
												<th>Date</th>
												<th>Status</th>
												<th>Amount</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>25-04-2019</td>
												<td><i class="icon ion-md-checkmark-circle-outline green"></i></td>
												<td>4.5454334</td>
											</tr>
											<tr>
												<td>2</td>
												<td>25-05-2019</td>
												<td><i class="icon ion-md-checkmark-circle-outline green"></i></td>
												<td>0.5484468</td>
											</tr>
											<tr>
												<td>3</td>
												<td>25-06-2019</td>
												<td><i class="icon ion-md-close-circle-outline red"></i></td>
												<td>2.5454545</td>
											</tr>
											<tr>
												<td>4</td>
												<td>25-07-2019</td>
												<td><i class="icon ion-md-checkmark-circle-outline green"></i></td>
												<td>1.45894147</td>
											</tr>
											<tr>
												<td>5</td>
												<td>25-08-2019</td>
												<td><i class="icon ion-md-close-circle-outline red"></i></td>
												<td>2.5454545</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div> -->
				</div>
			</div>
		<?php } ?>

	</div>
</div>
</div>

<?php include(__DIR__ . '/frontend/includes/footer.php') ?>

<?php if (!isset($_SESSION['stock_details']['stock_symbol']) || empty($_SESSION['stock_details']['stock_symbol'])) { ?>
	<script>
		$('.tab button:first').addClass('active');
		$('div.tabcontent:first').addClass('active');



		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();
	</script>
<?php } ?>
<script>
	function openStock(evt, StockTabName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
			tabcontent[i].removeAttribute("hidden");
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(StockTabName).style.display = "block";
		evt.currentTarget.className += " active";
		document.getElementById(StockTabName).removeAttribute("hidden");
	}
	$('div.tabcontent:not(.active)').attr('hidden', true);
</script>
<script>
	var tab = '';
	$(document).on('click', '.tablinks', function() {
		tab = $(this).attr('data-tab-id');
		var symbol = $(this).attr('data-symbol');
		am4core.useTheme(am4themes_animated);
		$.post('frontend/ajax/getStockChartDetails.php', {
			symbol: symbol
		}, function(result) {
			var chart = am4core.create("chartdiv" + tab, am4charts.XYChart);
			chart.paddingRight = 20;

			var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
			dateAxis.renderer.grid.template.location = 0;
			dateAxis.groupData = true;
			//dateAxis.skipEmptyPeriods = true;

			var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
			// valueAxis.tooltip.disabled = true;

			var series = chart.series.push(new am4charts.CandlestickSeries());
			series.dataFields.dateX = "date";
			series.dataFields.valueY = "close";
			series.dataFields.openValueY = "open";
			series.dataFields.lowValueY = "low";
			series.dataFields.highValueY = "high";
			series.tooltipText = "Open:${openValueY.value}\nLow:${lowValueY.value}\nHigh:${highValueY.value}\nClose:${valueY.value}";

			// important!
			// candlestick series colors are set in states. 
			// series.riseFromOpenState.properties.fill = am4core.color("#00ff00");
			// series.dropFromOpenState.properties.fill = am4core.color("#FF0000");
			// series.riseFromOpenState.properties.stroke = am4core.color("#00ff00");
			// series.dropFromOpenState.properties.stroke = am4core.color("#FF0000");

			series.riseFromPreviousState.properties.fillOpacity = 1;
			series.dropFromPreviousState.properties.fillOpacity = 0;

			chart.cursor = new am4charts.XYCursor();
			// chart.cursor.behavior = "panX";

			// a separate series for scrollbar
			var lineSeries = chart.series.push(new am4charts.LineSeries());
			lineSeries.dataFields.dateX = "date";
			lineSeries.dataFields.valueY = "close";
			// need to set on default state, as initially series is "show"
			lineSeries.defaultState.properties.visible = false;

			// hide from legend too (in case there is one)
			lineSeries.hiddenInLegend = true;
			lineSeries.fillOpacity = 0.5;
			lineSeries.strokeOpacity = 0.5;

			var scrollbarX = new am4charts.XYChartScrollbar();
			scrollbarX.series.push(lineSeries);
			chart.scrollbarX = scrollbarX;

			chart.data = JSON.parse(result);


		});



	});
$(document).ready(function(){
	var symbol = "ORCL";
		if("<?php echo (isset($_SESSION['stock_details']['stock_symbol']) && !empty($_SESSION['stock_details']['stock_symbol']) ? $_SESSION['stock_details']['stock_symbol'] : "") ?>" != "" ){
          symbol = "<?php echo (isset($_SESSION['stock_details']['stock_symbol']) && !empty($_SESSION['stock_details']['stock_symbol']) ? $_SESSION['stock_details']['stock_symbol'] : "") ?>";
		}
var tab_id = $('div.tabcontent.active').attr('data-inc-id');
	$.post('frontend/ajax/getStockChartDetails.php', {
			symbol: symbol
		}, function(data) {
			var chart = am4core.create("chartdiv" + tab_id, am4charts.XYChart);
			chart.paddingRight = 20;

			var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
			dateAxis.renderer.grid.template.location = 0;
			dateAxis.groupData = true;
			//dateAxis.skipEmptyPeriods = true;

			var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
			// valueAxis.tooltip.disabled = true;

			var series = chart.series.push(new am4charts.CandlestickSeries());
			series.dataFields.dateX = "date";
			series.dataFields.valueY = "close";
			series.dataFields.openValueY = "open";
			series.dataFields.lowValueY = "low";
			series.dataFields.highValueY = "high";
			series.tooltipText = "Open:${openValueY.value}\nLow:${lowValueY.value}\nHigh:${highValueY.value}\nClose:${valueY.value}";

			// important!
			// candlestick series colors are set in states. 
			// series.riseFromOpenState.properties.fill = am4core.color("#00ff00");
			// series.dropFromOpenState.properties.fill = am4core.color("#FF0000");
			// series.riseFromOpenState.properties.stroke = am4core.color("#00ff00");
			// series.dropFromOpenState.properties.stroke = am4core.color("#FF0000");

			series.riseFromPreviousState.properties.fillOpacity = 1;
			series.dropFromPreviousState.properties.fillOpacity = 0;

			chart.cursor = new am4charts.XYCursor();
			chart.cursor.behavior = "panX";

			// a separate series for scrollbar
			var lineSeries = chart.series.push(new am4charts.LineSeries());
			lineSeries.dataFields.dateX = "date";
			lineSeries.dataFields.valueY = "close";
			// need to set on default state, as initially series is "show"
			lineSeries.defaultState.properties.visible = false;

			// hide from legend too (in case there is one)
			lineSeries.hiddenInLegend = true;
			lineSeries.fillOpacity = 0.5;
			lineSeries.strokeOpacity = 0.5;

			var scrollbarX = new am4charts.XYChartScrollbar();
			scrollbarX.series.push(lineSeries);
			chart.scrollbarX = scrollbarX;

			chart.data = JSON.parse(data);
		});
})
		
</script>


</body>

</html>