-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2021 at 09:56 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment_online_equity`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `wallet_amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `user_id`, `first_name`, `last_name`, `contact_no`, `age`, `gender`, `email`, `address`, `wallet_amount`) VALUES
(1, 1, 'Joe', 'Kaul', '56565579', 27, 'Female', 'joe.45@gmail.com', '67  Fulford Road', '140.50'),
(2, 2, 'Elliot', 'Bruce', '(07) 4998 8876', 26, 'Female', 'x1ddy11kne@temporary-mail.net', '42  Station Rd', '300.00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_stocks`
--

CREATE TABLE `customer_stocks` (
  `customer_stock_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `stock_price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_stocks`
--

INSERT INTO `customer_stocks` (`customer_stock_id`, `customer_id`, `stock_id`, `stock_price`, `quantity`, `total_price`, `create_date`) VALUES
(1, 1, 10, '5.50', 10, '55.00', '2021-03-31 12:32:30'),
(2, 1, 8, '13.18', 0, '131.80', '2021-04-01 14:29:15');

-- --------------------------------------------------------

--
-- Table structure for table `customer_stock_activities`
--

CREATE TABLE `customer_stock_activities` (
  `customer_stock_activity_id` int(11) NOT NULL,
  `customer_stock_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_stock_activities`
--

INSERT INTO `customer_stock_activities` (`customer_stock_activity_id`, `customer_stock_id`, `customer_id`, `action`, `quantity`, `total_price`, `create_date`) VALUES
(1, 1, 1, 1, 10, '55.00', '2021-03-31 12:32:31'),
(2, 2, 1, 1, 10, '131.80', '2021-04-01 14:29:15'),
(3, 2, 1, 2, 10, '127.30', '2021-04-02 07:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) NOT NULL,
  `images` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `name`, `symbol`, `images`, `created_at`) VALUES
(1, 'Oracle Corp.', 'ORCL', 'oracle-logo.jpg', '2021-03-22 11:43:25'),
(2, 'Canadian National Railway Co.', 'CNI', 'CN_Railway_logo.png', '2021-03-22 11:43:25'),
(3, 'Apple Inc.', 'AAPL', 'apple.png', '2021-03-22 11:46:26'),
(4, 'FBL Financial Group Inc.', 'FFG', 'FBL-logo.jpg', '2021-03-22 11:46:26'),
(5, 'Alphabet Inc. Cl A', 'GOOGL', 'alphabet-inc-logo.png', '2021-03-22 11:48:26'),
(6, 'BlackBerry Ltd.', 'BB', 'blackberry.png', '2021-03-22 11:46:26'),
(7, 'Intel Corp.', 'INTC', 'Intel-Logo.png', '2021-03-22 11:54:26'),
(8, 'Foghorn Therapeutics Inc.', 'FHTX', 'foghorn-therapeutics.png', '2021-03-22 11:54:26'),
(9, 'Microsoft Corp.', 'MSFT', 'image-microsoft.png', '2021-03-22 11:54:32'),
(10, 'Hall of Fame Resort & Entertainment Co.', 'HOFV', 'hall-of-fame.jpg', '2021-03-22 11:56:29'),
(11, 'Caesars Entertainment Inc.', 'CZR', 'Caesars_Entertainment_Corporation-Logo.png', '0000-00-00 00:00:00'),
(12, 'National Western Life Group Inc. Cl A', 'NWLI', 'national-western-logo.png', '2021-03-22 11:59:04'),
(13, 'Edgewell Personal Care Co.', 'EPC', 'edgewell-logo-large.png', '2021-03-22 12:01:04'),
(14, 'Atlantic American Corp.', 'AAME', 'atlantic-american-corp-logo.png', '2021-03-22 12:01:04'),
(15, 'Allied Motion Technologies Inc.', 'AMOT', 'allied-motion-logo.png', '2021-03-22 12:02:04'),
(16, 'CEL-SCI Corp.', 'CVM', 'cel-sci-logo.png', '2021-03-22 12:12:04'),
(17, 'BWX Technologies Inc.', 'BWXT', 'BWXT_Technologies_logo.png', '2021-03-22 12:13:04'),
(18, 'SenesTech Inc.', 'SNES', 'senestech-logo.jfif', '2021-03-22 12:14:04'),
(19, 'Generac Holdings Inc.', 'GNRC', 'generac-logo.png', '2021-03-22 12:15:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `encrypted_password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `role_id`, `name`, `username`, `password`, `encrypted_password`) VALUES
(1, 2, 'Joe', 'joe', '123', '202cb962ac59075b964b07152d234b70'),
(2, 2, 'Elliot', 'elliot', '123', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_history`
--

CREATE TABLE `wallet_history` (
  `wallet_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_stock_activity_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `create_date` datetime NOT NULL,
  `action` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet_history`
--

INSERT INTO `wallet_history` (`wallet_history_id`, `customer_id`, `customer_stock_activity_id`, `type`, `amount`, `create_date`, `action`) VALUES
(1, 1, NULL, 2, '200.00', '2021-03-31 12:28:21', 'Amount added to wallet'),
(2, 1, 1, 1, '55.00', '2021-03-31 12:32:31', 'Bought Share - HOFV'),
(3, 1, 2, 1, '131.80', '2021-04-01 14:29:16', 'Bought Share - FHTX'),
(4, 1, 3, 2, '127.30', '2021-04-02 07:06:40', 'Share sold - FHTX'),
(5, 2, NULL, 2, '300.00', '2021-04-02 09:52:54', 'Amount added to wallet');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_stocks`
--
ALTER TABLE `customer_stocks`
  ADD PRIMARY KEY (`customer_stock_id`);

--
-- Indexes for table `customer_stock_activities`
--
ALTER TABLE `customer_stock_activities`
  ADD PRIMARY KEY (`customer_stock_activity_id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `wallet_history`
--
ALTER TABLE `wallet_history`
  ADD PRIMARY KEY (`wallet_history_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_stocks`
--
ALTER TABLE `customer_stocks`
  MODIFY `customer_stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_stock_activities`
--
ALTER TABLE `customer_stock_activities`
  MODIFY `customer_stock_activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wallet_history`
--
ALTER TABLE `wallet_history`
  MODIFY `wallet_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
