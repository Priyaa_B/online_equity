<?php
session_start();
include(__DIR__ . "/frontend/classes/SqlQueries.php");
$query = new SqlQueries();
require_once(__DIR__ . '\frontend/ajax/getStocks.php');
require_once(__DIR__ . '\frontend/ajax/getSingleStockList.php');
?>

<?php include(__DIR__ . '/frontend/includes/head.php') ?>
<?php include(__DIR__ . '/frontend/includes/header.php') ?>


<div class="container-fluid mtb15 no-fluid">
	<div class="row sm-gutters">
		<div class="col-lg-3 col-md-6">
			<div class="data-tab">
					<h2 class="text-center">Stocks</h2>
				

				<div class="data-table table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Pairs</th>
								<th>Last Price</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($stock_list as $stocks) { ?>
								<tr>
									<td><a href="index.php?stock_symbol=<?php echo $stocks['symbol'] ?>"><?php echo $stocks['stock_name'] ?></a></td>
									<td><?php echo $stocks['c'] ?></td>

								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>


		</div>
		<div class="col-lg-6 col-md-6">
			<div class="data-box">
				<div class="data-hd">
					<?php
					$icon = (($stock_latest_price['ch'] <=> 0) == 1) ? 'fa fa-caret-up' : 'fa fa-caret-down';
					$color = (($stock_latest_price['ch'] <=> 0) == 1) ? 'text-success' : 'text-danger';
					?>
					<h1><?php echo $stockName; ?></h1>
					<p>
						 <span class="text-info"><?php echo $stock_latest_price['c'] ?></span>C
						 <span class="text-info"><?php echo $stock_latest_price['h'] ?></span>
						H <span class="text-info"><?php echo $stock_latest_price['l'] ?></span>
						L <span class="<?php echo $color ?> ml-1"><i class="<?php echo $icon ?>" aria-hidden="true"></i><?php echo $stock_latest_price['ch'] ?>&nbsp;<?php echo $stock_latest_price['cp'] ?></span></p>
					<!-- <div id="chartContainer" style="height: 370px; width: 100%;"></div> -->
					<hr>
					<div id="chartdiv" style="height: 420px; width: 100%;"></div>
					<div class="walContBox text-center">

						<!-- <div class="d-flex justify-content-between"> -->
							<form method="post" action="stock_buy_sell.php">
							<div class="market-trade-buy">
								<input type="hidden" name="stock_price" id="stock_price" value="<?php echo $stock_latest_price['c']  ?>"/>
								<input type="hidden" name="stock_symbol" id="stock_symbol" value="<?php echo$stock_company_profile['ticker']  ?>"/>
								<!-- <div class="form-group">
								<div class="input-group">
									<input type="number" class="form-control" name="buy_total_price" id="buy_total_price" placeholder="Price" readonly>
									<div class="input-group-append">
										<span class="input-group-text">$</span>
									</div>
								</div>
								</div>
								<div class="form-group">
								<div class="input-group">
									<input type="number" class="form-control" name="buy_quantity" id="buy_quantity" placeholder="Amount">
									<div class="input-group-append">
										<span class="input-group-text">No.</span>
									</div>
								</div>
								</div> -->
								<button name="stock_buy" class="btn green form-control p-0 mt-4">Buy Now</button>
							</div>
							</form>
							<!-- <form method="post" action="stock_buy_sell.php">
							<div class="market-trade-sell">
								<div class="form-group">
								<div class="input-group">
									<input type="number" class="form-control" placeholder="Price">
									<div class="input-group-append">
										<span class="input-group-text">$</span>
									</div>
								</div>
								</div>
								<div class="form-group">
								<div class="input-group">
									<input type="number" class="form-control" placeholder="Amount">
									<div class="input-group-append">
										<span class="input-group-text">No.</span>
									</div>
								</div>
								</div>
								<button name="stock_sell" class="btn red">Sell</button>
							</div>
							</form> -->
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-12">
			<div class="proifle-tab">
				<h2>Copmpany Profile</h2>
				<div class="profile-img">
					<img src="<?php echo $stock_company_profile['logo'] ?>" width="100">
				</div>
				<ul>
					<li>
						<p><span class="font-weight-bold">Name :</span> <?php echo $stock_company_profile['name'] ?></p>
					</li>
					<li>
						<p><span class="font-weight-bold">Symbol :</span> <?php echo $stock_company_profile['ticker'] ?></p>
					</li>
					<li>
						<p><span class="font-weight-bold">Website :</span> <?php echo $stock_company_profile['weburl'] ?></p>
					</li>
					<li>
						<p><span class="font-weight-bold">Mobile :</span> <?php echo $stock_company_profile['phone'] ?></p>
					</li>
					<li>
						<p><span class="font-weight-bold">Exchange :</span> <?php echo $stock_company_profile['exchange'] ?></p>
					</li>
					<li>
						<p><span class="font-weight-bold">Industry :</span> <?php echo $stock_company_profile['finnhubIndustry'] ?></p>
					</li>
					<li>
						<p><span class="font-weight-bold">MarketCap. :</span> <?php echo $stock_company_profile['marketCapitalization'] ?></p>
					</li>
					<li>
						<p><span class="font-weight-bold">Share Outstanding. :</span> <?php echo $stock_company_profile['shareOutstanding'] ?></p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid mtb15 no-fluid">
	<div class="row sm-gutters">
		<div class="col-md-12">
			<div class="market-news">
				<h2 class="heading">Market News</h2>
				<div class="p-body">
					<?php for ($i = 1; $i <= 5; $i++) {
						if(isset($company_news[$i]['headline']) && !empty($company_news[$i]['headline'])){ ?>
						<div class="row">
							<div class="col-md-1 mb-2">
								<img class="img-thumbnail" src="<?php echo $company_news[$i]['image']; ?>" width="100px" height="80px">
							</div>
							<div class="col-md-10">
								<strong><?php echo $company_news[$i]['headline'] ?></strong><br>
								<p class="parag"><?php echo $company_news[$i]['summary']; ?><br><span class="date"><?php echo date('d/m/Y', $company_news[$i]['datetime']); ?></span></p>
							</div>
						</div>
					<?php } } ?>

				</div>
			</div>
		</div>
	</div>
</div>
<?php include(__DIR__ . '/frontend/includes/footer.php') ?>
<script>
	am4core.useTheme(am4themes_animated);

	var chart = am4core.create("chartdiv", am4charts.XYChart);
	chart.paddingRight = 20;

	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.grid.template.location = 0;
	dateAxis.groupData = true;
	//dateAxis.skipEmptyPeriods = true;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.tooltip.disabled = true;

	var series = chart.series.push(new am4charts.CandlestickSeries());
	series.dataFields.dateX = "date";
	series.dataFields.valueY = "close";
	series.dataFields.openValueY = "open";
	series.dataFields.lowValueY = "low";
	series.dataFields.highValueY = "high";
	series.tooltipText = "Open:${openValueY.value}\nLow:${lowValueY.value}\nHigh:${highValueY.value}\nClose:${valueY.value}";

	// important!
	// candlestick series colors are set in states. 
	// series.riseFromOpenState.properties.fill = am4core.color("#00ff00");
	// series.dropFromOpenState.properties.fill = am4core.color("#FF0000");
	// series.riseFromOpenState.properties.stroke = am4core.color("#00ff00");
	// series.dropFromOpenState.properties.stroke = am4core.color("#FF0000");

	series.riseFromPreviousState.properties.fillOpacity = 1;
	series.dropFromPreviousState.properties.fillOpacity = 0;

	chart.cursor = new am4charts.XYCursor();
	chart.cursor.behavior = "panX";

	// a separate series for scrollbar
	var lineSeries = chart.series.push(new am4charts.LineSeries());
	lineSeries.dataFields.dateX = "date";
	lineSeries.dataFields.valueY = "close";
	// need to set on default state, as initially series is "show"
	lineSeries.defaultState.properties.visible = false;

	// hide from legend too (in case there is one)
	lineSeries.hiddenInLegend = true;
	lineSeries.fillOpacity = 0.5;
	lineSeries.strokeOpacity = 0.5;

	var scrollbarX = new am4charts.XYChartScrollbar();
	scrollbarX.series.push(lineSeries);
	chart.scrollbarX = scrollbarX;

	chart.data = <?php echo json_encode($single_stock_list); ?>

	$(document).on('keyup','#buy_quantity',function(){
		var quantity = parseFloat($(this).val());
		var stock_rice = parseFloat($('#stock_price').val());
		var total_price = quantity * stock_rice;
		$('#buy_total_price').val(total_price);
	})
</script>
</body>

</html>