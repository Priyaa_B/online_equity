<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="frontend\assets\bootstrap5\css\bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="frontend\assets\jquery\jquery.min.js"></script>
    <script type="text/javascript" src="frontend\assets\bootstrap5\js\bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <form class="row g-3">

        <div class="col-md-6">
            <label for="inputCity" class="form-label">City</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
        <div class="col-md-4">
            <label for="inputState" class="form-label">Stocks</label>
            <select id="stocks" class="form-select">
                <option selected>Choose...</option>
                <option>...</option>
            </select>
        </div>
        <div class="col-12">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Check me out
                </label>
            </div>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary">Sign in</button>
        </div>
    </form>
</div>
<script>
$(document).
</script>
</body>

</html>