<?php
session_start();
include(dirname(__DIR__) . "/frontend/classes/SqlQueries.php");
$query = new SqlQueries();
// unset( $_SESSION['stock_list']);
include(dirname(__DIR__) . "/user/user_auth.php");
if(isset($_POST['add_amount'])){
	$_SESSION['u_time'] = time();
    $wallet_amount = $query->SelectSingle("SELECT * FROM customers WHERE customer_id=".$_SESSION['user_session']['customer_id']);
    $total_amount = $wallet_amount['wallet_amount'] + $_POST['wallet_amount'];
    $wallet = $query->UpdateQuery('UPDATE customers SET wallet_amount='.$total_amount.' WHERE customer_id='.$_SESSION['user_session']['customer_id']);
    $wallet_history_id = $query->InsertQuery("INSERT INTO wallet_history SET customer_id =".$_SESSION['user_session']['customer_id'].",type=".TYPE_DEPOSITED.", amount=".$_POST['wallet_amount'].",action='".DEPOSIT_TEXT."',create_date='" . date('Y-m-d H:i:s') . "' ");
    if($wallet){
        $_SESSION['level'] = 'success';
		$_SESSION['message'] = 'Amount added successfully';
	} else {
		$_SESSION['level'] = 'danger';
		$_SESSION['message'] = 'Amount cannot be added';
		$message = "Record cannot be added";
	}
    redirect('wallet.php');
}


if(isset($_SESSION['user_session']) && !empty($_SESSION['user_session'])){
    $customer_wallet = $query->SelectSingle("SELECT * FROM customers WHERE customer_id=".$_SESSION['user_session']['customer_id']);
    $customer_wallet_history = $query->SelectQuery("SELECT * FROM wallet_history WHERE customer_id=".$_SESSION['user_session']['customer_id']." ORDER BY create_date DESC");
}

?>
<?php include(dirname(__DIR__) . '/frontend/includes/head.php') ?>
<?php include(dirname(__DIR__) . '/frontend/includes/user_header.php') ?>
<style>
    p.wallet-amount {
        font-size: 4em;
    }

    label.amount-add {
        font-size: 28px;
    }
</style>
<div class="container mtb15 no-fluid">
    <div class="row sm-gutters border">
        <div class="col-md-12 col-lg-12 ml-3">
            <h2>BALANCE</h2>
        </div>
        <div class="col-md-12 col-lg-12 ml-5 p-5">
            <h5>Balance Available :</h5>
            <p class="wallet-amount"><?php echo $customer_wallet['wallet_amount'] ?></p>
        </div>
    </div>
    <div class="row sm-gutters border mt-1 p-3">
        <div class="col-md-6 col-lg-6 ">
            <label class="amount-add">Amount to add:</label>
        </div>
        <div class="col-md-6 col-lg-6 ">
            <form method="post">
            <input type="number" name="wallet_amount" class="form-control bg-light" required />
            <button type="submit" class="form-control btn btn-primary mt-1 " name="add_amount">Add</button>
            </form>
        </div>

    </div>
    <div class="row sm-gutters border mt-1 p-3">
        <div class="col-md-12 col-lg-12 mb-4">
            <h3>Details</h3>
        </div>
        <div class="col-md-12 col-lg-12 ">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">S. NO.</th>
                            <th scope="col">Action</th>
                            <th scope="col">Type</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i=  1;
                        if(count($customer_wallet_history) >0 ){
                         foreach($customer_wallet_history as $history){
                        ?>
                        <tr>
                            <th scope="row"><?php echo $i++ ?></th>
                            <td><?php echo $history['action'] ?></td>
                            <?php if($history['type'] == TYPE_WITHDRAWN){ ?>
                            <td class="text-danger"><?php echo $history_type[$history['type']] ?></td>
                            <td class="text-danger"><?php echo '-'.$history['amount'] ?></td>
                            <?php }elseif($history['type'] == TYPE_DEPOSITED){ ?>
                            <td class="text-success"><?php echo $history_type[$history['type']] ?></td>
                            <td class="text-success"><?php echo '+'.$history['amount'] ?></td>
                            <?php } ?>
                           <td><?php echo date('d-m-Y H:I:s',strtotime($history['create_date'])) ?></td>
                        </tr>
                        <?php } 
                    }else{
                    ?>
                    <td colspan="5" class="text-center">
                           No Records Found.
                    </td>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<?php include(dirname(__DIR__) . '/frontend/includes/footer.php') ?>