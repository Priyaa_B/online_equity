<?php
session_start();
// unset( $_SESSION['stock_list']);
$customerstock_id = $_GET['id'];
if (isset($_SESSION['stock_list']['date']) && $_SESSION['stock_list']['date'] <= strtotime(date("Y-m-d H:i:s"))) {
	// echo "unseting";
	// exit;
	unset($_SESSION['stock_list']);
}
include(dirname(__DIR__). "/frontend/classes/SqlQueries.php");
$query = new SqlQueries();
include(dirname(__DIR__) . "/user/user_auth.php");
$stocks_listing = array();
if (!isset($_SESSION['stock_list']) || count($_SESSION['stock_list']) == 0) {
	require_once(dirname(__DIR__). '\frontend/ajax/getStocks.php');
	$_SESSION['stock_list'] = $stock_list;
	$_SESSION['stock_list']['date'] = strtotime(date('Y-m-d') . "23:59:59");
}
$stocks_listing = $_SESSION['stock_list'];

//customer details
if (isset($_POST['sell_stock'])) {
	$_SESSION['u_time'] = time();

    $customer_stock_details = $query->SelectSingle("SELECT * FROM customer_stocks WHERE customer_stock_id=".$_POST['customer_stock_id']);
    
    if($customer_stock_details['quantity'] == 0){
        $_SESSION['level'] = 'danger';
		$_SESSION['message'] = 'You have no shares to sell';
        redirect('portfolio.php');

    }  
    elseif($customer_stock_details['quantity'] < $_POST['no_of_shares']){
        $_SESSION['level'] = 'danger';
		$_SESSION['message'] = 'Number of shares are larger then bought';
        redirect('portfolio.php');

    }else{
	$total_price = $_POST['no_of_shares'] * $_POST['stock_price'];

   $customer_details = $query->SelectSingle("SELECT * FROM customers WHERE customer_id=".$_POST['customer_id']);
    $stock_details = $query->SelectSingle("SELECT * FROM stocks WHERE id=".$_POST['stock_id']);
	

	$customer_stock_activity_id = $query->InsertQuery('INSERT INTO customer_stock_activities SET customer_stock_id="' . $_POST['customer_stock_id'] . '", customer_id="' . $_POST['customer_id'] . '",action =' . ACTION_SELL . ', quantity="' . $_POST['no_of_shares'] . '", total_price="' . $total_price . '", create_date="' . date('Y-m-d H:i:s') . '"');
	   $left_amount = $customer_details['wallet_amount'] + $total_price; 
       $shares_left = $customer_stock_details['quantity'] - $_POST['no_of_shares'];
	   $query->UpdateQuery("UPDATE customer_stocks SET quantity =".$shares_left." WHERE customer_stock_id=".$_POST['customer_stock_id']);
	   $query->UpdateQuery("UPDATE customers SET wallet_amount =".$left_amount." WHERE customer_id=".$_POST['customer_id']);
	   $query->InsertQuery("INSERT INTO wallet_history SET customer_id =".$_POST['customer_id'].", customer_stock_activity_id=".$customer_stock_activity_id.",type=".TYPE_DEPOSITED.", amount=".$total_price.",action='".STOCK_SELL_DEPOSITED_TEXT.' '.$stock_details['symbol']."',create_date='" . date('Y-m-d H:i:s') . "'");
      
	   if (!empty($customer_stock_activity_id)) {
		$_SESSION['level'] = 'success';
		$_SESSION['message'] = 'Stock sold successfully';
        } else {
            $_SESSION['level'] = 'danger';
            $_SESSION['message'] = 'Record cannot be added';
            $message = "Stock cannot be sold";
        }
	redirect('portfolio.php');
    }
		
}

$customer_stock = $query->SelectSingle("SELECT * ,stocks.symbol FROM customer_stocks INNER JOIN stocks ON stocks.id = customer_stocks.stock_id WHERE customer_stock_id=".$customerstock_id);


?>
<?php include(dirname(__DIR__). '/frontend/includes/head.php') ?>
<?php include(dirname(__DIR__). '/frontend/includes/user_header.php') ?>

<div class="container-fluid mtb15 no-fluid">
	<?php include(dirname(__DIR__).'/frontend/includes/alert.php') ?>
	<div class="row sm-gutters">
		<div class="col-md-12 col-lg-4">
			<div class="tab overflow-auto">
				<?php
				$i = 1;
				foreach ($stocks_listing as $key => $stocks) { ?>
					<button class="<?php echo (isset($_SESSION['stock_details']['stock_symbol']) && $stocks['symbol'] == $_SESSION['stock_details']['stock_symbol'] ? 'tablinks active' : 'tablinks') ?>" onclick="openStock(event, 'Tab<?php echo $i++ ?>')" id=<?php echo $key == 1 ? "defaultOpen" : "" ?>>
						<div class="walBox d-flex justify-content-between align-items-center">
							<div class="d-flex">
								<img src="<?php echo BASE_PATH ?>/frontend/assets/imgs/stock_logos/<?php echo $stocks['logo'] ?>">
								<div>
									<h2><?php echo $stocks['symbol'] ?></h2>
									<p><?php echo $stocks['stock_name'] ?></p>
								</div>
							</div>
							<div>
								<h3><?php echo $stocks['c'] ?></h3>
								<!-- <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p> -->
							</div>
						</div>
					</button>
					<hr>
				<?php } ?>
			</div>
		</div>
		<div class="col-md-12 col-lg-8">
				<div id="Tab" class="tabcontent">
					<div class="walContBox">
						<div class="card">
							<form method="post">
								<div class="card-body">
									<h5 class="card-title"><?php echo $customer_stock['symbol'] ?></h5>
									<ul>
										<li class="d-flex justify-content-between align-items-center">
											<div class="d-flex align-items-center">
												<i class="icon ion-md-cash"></i>
												<h2>Shares</h2>
											</div>
											<div>
												<input class="form-control" type="hidden" name="customer_stock_id" value="<?php echo $customer_stock['customer_stock_id'] ?>" />
												<input class="form-control" type="hidden" name="stock_id" value="<?php echo $customer_stock['stock_id'] ?>" />
												<input class="form-control" type="hidden" name="customer_id" value="<?php echo $_SESSION['user_session']['customer_id'] ?>" />
												<input class="form-control" type="number" name="no_of_shares" value="<?php echo $customer_stock['quantity'] ?>" required />
											</div>
										</li>
										<li class="d-flex justify-content-between align-items-center">
											<div class="d-flex align-items-center">
												<i class="icon ion-md-checkmark"></i>
												<h2>Price</h2>
											</div>
											<div>
                                                <?php foreach($stocks_listing as $s_list) {
                                                    if($s_list['symbol'] == $customer_stock['symbol'] ){
                                                    ?>
												<input class="form-control" type="hidden" name="stock_price" value="<?php echo $s_list['c'] ?>" />
												<h3><?php echo $s_list['c'] ?> <?php echo $s_list['symbol'] ?></h3>
                                                <?php }} ?>
											</div>
										</li>
									</ul>
									<button type="submit" name="sell_stock" class="form-control btn red">Sell</button>
								</div>
							</form>
						</div>
						<!-- <div class="card">
							<div class="card-body">
								<h5 class="card-title">Wallet Deposit Address</h5>
								<div class="row wallAddr">
									<div class="col-md-9">
										<p>Deposits to this address are unlimited. Note that you may not be able to withdraw all of your funds at once if you deposit more than your daily withdrawal limit.</p>
										<div class="input-group">
											<input type="text" class="form-control" value="Ad8">
											<div class="input-group-prepend">
												<button class="btn btn-primary">COPY</button>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<img src="imgs/qr-code.jpg" alt="" class="img-fluid">
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">Latest Transactions</h5>
								<div class="wallet-history">
									<table class="table">
										<thead>
											<tr>
												<th>No.</th>
												<th>Date</th>
												<th>Status</th>
												<th>Amount</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>25-04-2019</td>
												<td><i class="icon ion-md-checkmark-circle-outline green"></i></td>
												<td>4.5454334</td>
											</tr>
											<tr>
												<td>2</td>
												<td>25-05-2019</td>
												<td><i class="icon ion-md-checkmark-circle-outline green"></i></td>
												<td>0.5484468</td>
											</tr>
											<tr>
												<td>3</td>
												<td>25-06-2019</td>
												<td><i class="icon ion-md-close-circle-outline red"></i></td>
												<td>2.5454545</td>
											</tr>
											<tr>
												<td>4</td>
												<td>25-07-2019</td>
												<td><i class="icon ion-md-checkmark-circle-outline green"></i></td>
												<td>1.45894147</td>
											</tr>
											<tr>
												<td>5</td>
												<td>25-08-2019</td>
												<td><i class="icon ion-md-close-circle-outline red"></i></td>
												<td>2.5454545</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div> -->
					</div>
				</div>

		</div>
	</div>
</div>

<?php include(dirname(__DIR__). '/frontend/includes/footer.php') ?>

<?php if (!isset($_SESSION['stock_details']['stock_symbol']) || empty($_SESSION['stock_details']['stock_symbol'])) { ?>
	<script>
		$('.tab button:first').addClass('active');
		$('div.tabcontent:first').addClass('active');



		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();
	</script>
<?php } ?>
<script>
	function openStock(evt, StockTabName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
			tabcontent[i].removeAttribute("hidden");
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(StockTabName).style.display = "block";
		evt.currentTarget.className += " active";
		document.getElementById(StockTabName).removeAttribute("hidden");
	}
	$('div.tabcontent:not(.active)').attr('hidden', true);
</script>



</body>

</html>