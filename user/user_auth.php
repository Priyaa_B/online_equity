<?php

if(!isset($_SESSION['user_session']) || $_SESSION['user_session']['role_id'] != CUSTOMER_ROLE_ID){
   echo "<script>alert('Please login to access this page. ')</script>";
   redirect(BASE_PATH."/index.php");
}