<?php
session_start();
include(dirname(__DIR__) . "/frontend/classes/SqlQueries.php");
$query = new SqlQueries();
include(dirname(__DIR__) . "/user/user_auth.php");
// unset( $_SESSION['stock_list']);
if (isset($_SESSION['stock_list']['date']) && $_SESSION['stock_list']['date'] <= strtotime(date("Y-m-d H:i:s"))) {
	// echo "unseting";
	// exit;
	unset($_SESSION['stock_list']);
}


$stocks_listing = array();
if (!isset($_SESSION['stock_list']) || count($_SESSION['stock_list']) == 0) {
	require_once(dirname(__DIR__) . '/frontend/ajax/getStocks.php');
	$_SESSION['stock_list'] = $stock_list;
	$_SESSION['stock_list']['date'] = strtotime(date('Y-m-d') . "23:59:59");
}
$stocks_listing = $_SESSION['stock_list'];

//customer details
if (isset($_POST['buy_stock'])) {
	unset($_SESSION['stock_details']['stock_symbol']);
	$_SESSION['u_time'] = time();

	$total_price = $_POST['no_of_shares'] * $_POST['stock_price'];

   $customer_wallet = $query->SelectSingle("SELECT * FROM customers WHERE customer_id=".$_POST['customer_id']);
   if($customer_wallet['wallet_amount'] == null || $customer_wallet['wallet_amount'] == 0 || $customer_wallet['wallet_amount'] == "" || $customer_wallet['wallet_amount'] < $total_price){
		$_SESSION['level'] = 'danger';
		$_SESSION['message'] = 'Insufficient balance please check';
		// redirect('wallet.php');
	}
   elseif($customer_wallet['wallet_amount'] >= $total_price){
     
	$stock_details = $query->SelectSingle("SELECT * FROM stocks WHERE id=".$_POST['stock_id']);

	$customer_stock_id = $query->InsertQuery('INSERT INTO customer_stocks SET customer_id="' . $_POST['customer_id'] . '", stock_id="' . $_POST['stock_id'] . '", stock_price="' . $_POST['stock_price'] . '", quantity="' . $_POST['no_of_shares'] . '", total_price="' . $total_price . '", create_date="' . date('Y-m-d H:i:s') . '"');
	$customer_stock_activity_id = $query->InsertQuery('INSERT INTO customer_stock_activities SET customer_stock_id="' . $customer_stock_id . '", customer_id="' . $_POST['customer_id'] . '",action =' . ACTION_BUY . ', quantity="' . $_POST['no_of_shares'] . '", total_price="' . $total_price . '", create_date="' . date('Y-m-d H:i:s') . '"');
	   $left_amount = $customer_wallet['wallet_amount'] - $total_price; 
	   $query->UpdateQuery("UPDATE customers SET wallet_amount =".$left_amount." WHERE customer_id=".$_POST['customer_id']);
	   $query->InsertQuery("INSERT INTO wallet_history SET customer_id =".$_POST['customer_id'].", customer_stock_activity_id=".$customer_stock_activity_id.",type=".TYPE_WITHDRAWN.", amount=".$total_price.",action='".WITHDRAWN_TEXT.' '.$stock_details['symbol']."',create_date='" . date('Y-m-d H:i:s') . "'");
      
	   if (!empty($customer_stock_activity_id)) {
		$_SESSION['level'] = 'success';
		$_SESSION['message'] = 'Stock bought successfully';
	} else {
		$_SESSION['level'] = 'danger';
		$_SESSION['message'] = 'Record cannot be added';
		$message = "Record cannot be added";
	}
	redirect('portfolio.php');
	
	}
}

$customer_stocks = $query->SelectQuery("SELECT *,stocks.symbol FROM customer_stocks INNER JOIN stocks ON stocks.id = customer_stocks.stock_id WHERE customer_id=".$_SESSION['user_session']['customer_id']);


?>
<?php include(dirname(__DIR__) . '/frontend/includes/head.php') ?>
<?php include(dirname(__DIR__) . '/frontend/includes/user_header.php') ?>

<div class="container mtb15 no-fluid">
	<?php include(dirname(__DIR__).'/frontend/includes/alert.php') ?>
	<div class="row sm-gutters">
		<div class="col-md-12 col-lg-12">
		<?php foreach ($customer_stocks as $key => $stocks) { 
			   if(!empty($stocks['quantity'])){
			?>
			<div class="row border">
				<div class="col-md-6">
					<h2><a href="sell.php?id=<?php echo $stocks['customer_stock_id']?>"><?php echo $stocks['symbol'] ?></a></h2>
		</div>
		<div class="col-md-6 text-right mt-2">
			<span class="align-middle mt-3 font-weight-bold">Current:</span>
		</div>
		<div class="col-md-6">
					<label class="text-muted">QTY :</label><span><?php echo $stocks['quantity'] ?></span> ( <label class="text-muted">Price/Qty :</label><span><?php echo $stocks['stock_price'] ?></span> )
		</div>
		<div class="col-md-6 text-right">
			<?php foreach ($stocks_listing as $key => $stockList) {
				if($stockList['symbol'] == $stocks['symbol']){
				?>
					<span class="align-middle"><?php echo $stockList['c'] ?></span>
				<?php }} ?>
		</div>
		<div class="col-md-12">
					<label class="text-muted">Invested :</label><span><?php echo $stocks['total_price'] ?></span>

				</div>
			</div>
			<?php } } ?>
		</div>
		
	</div>
</div>

<?php include(dirname(__DIR__) . '/frontend/includes/footer.php') ?>

<?php if (!isset($_SESSION['stock_details']['stock_symbol']) || empty($_SESSION['stock_details']['stock_symbol'])) { ?>
	<script>
		$('.tab button:first').addClass('active');
		$('div.tabcontent:first').addClass('active');



		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();
	</script>
<?php } ?>
<script>
	function openStock(evt, StockTabName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
			tabcontent[i].removeAttribute("hidden");
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(StockTabName).style.display = "block";
		evt.currentTarget.className += " active";
		document.getElementById(StockTabName).removeAttribute("hidden");
	}
	$('div.tabcontent:not(.active)').attr('hidden', true);
</script>



</body>

</html>