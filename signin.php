<?php
session_start();
include(__DIR__ . '/frontend/classes/SqlQueries.php');
$sqlQuery = new SqlQueries();

if (isset($_SESSION['user_session'])) {
    redirect('index.php');
}

if (isset($_POST['login'])) {
    $user_details = $sqlQuery->SelectSingle("Select users.*,customers.customer_id, customers.first_name from users left join customers on customers.user_id = users.user_id where users.username='" . $_POST['username'] . "' and users.encrypted_password='" . md5($_POST['password']) . "'");
   
	if ($user_details) {
        $_SESSION['user_session'] = $user_details;
        echo "<script>alert('Login Successfully.')</script>";
    } else {
        echo "<script>alert('Invalid username or password.')</script>";
        redirect('login_register.php');
    }
// print_r($_SESSION['stock_details']);
// exit;
    if (isset($_SESSION['stock_details'])) {
       
            redirect(BASE_PATH.'/user_index.php');
       
    } else {
        redirect('index.php');
    }
}
?>
<?php include(__DIR__ . '/frontend/includes/head.php') ?>
<?php include(__DIR__ . '/frontend/includes/header.php') ?>


<div class="d-flex justify-content-center">
	<div class="signUp my-auto">
		<form method="post">
			<span>Create Account</span>
			<div class="form-group">
				<input type="text" class="form-control" name="username" placeholder="Username">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="password" placeholder="Password">
			</div>
			
			<button type="submit" name="login" class="btn btn-primary">Sign In</button>
		</form>
		<h2>Don't have an account? <a href="signup.php">Sign up here</a></h2>
	</div>
</div>


<?php include(__DIR__ . '/frontend/includes/footer.php') ?>
</body>
</html>